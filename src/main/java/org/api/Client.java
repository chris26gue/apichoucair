package org.api;

import com.google.gson.Gson;
import io.restassured.http.ContentType;
import net.serenitybdd.rest.SerenityRest;
import io.restassured.response.Response;
import org.api.models.ApiResponseModel;

public class Client {

    /**
     * Base endpoint URL for the REST API. All API requests are constructed using this base URL.
     * The API used in this context is a dummy REST API example.
     */
    private final String endpointBase = "https://dummy.restapiexample.com/api/v1/";

    private final Gson gson = new Gson();

    /**
     * Retrieves information for a specific employee based on the provided ID.
     *
     * @param id The unique identifier of the employee.
     */
    public void getEmployeeInformation(String id) {
        String endpoint = endpointBase + "employee/" +id;

        SerenityRest.given()
                .when()
                .get(endpoint);
    }

    /**
     * Retrieves a list of employees.
     */
    public void getEmployees() {
        String endpoint = endpointBase + "employees";

        SerenityRest.given()
                .when()
                .get(endpoint);
    }

    /**
     * Creates a new record with the provided name, salary, and age.
     *
     * @param name   The name of the employee.
     * @param salary The salary of the employee.
     * @param age    The age of the employee.
     * @return A Response object representing the result of the POST request.
     */
    public void createNewRecord(String name, String salary, String age) {
        String endpoint = endpointBase + "create";

        ApiResponseModel apiResponseModel = new ApiResponseModel(name, salary, age);
        String jsonBody = gson.toJson(apiResponseModel);

        SerenityRest.given()
                .contentType(ContentType.JSON)
                .body(jsonBody)
                .when()
                .post(endpoint);
    }

    /**
     * Updates a record with the provided ID, name, salary, and age.
     *
     * @param id     The unique identifier of the record to be updated.
     * @param name   The updated name of the record.
     * @param salary The updated salary of the record.
     * @param age    The updated age of the record.
     * @return A Response object representing the result of the PUT request.
     */
    public void updateRecord(String id,String name, String salary, String age) {
        String endpoint = endpointBase + "update/" + id;

        ApiResponseModel apiResponseModel = new ApiResponseModel(name, salary, age);
        String jsonBody = gson.toJson(apiResponseModel);

        SerenityRest.given()
                .contentType(ContentType.JSON)
                .body(jsonBody)
                .when()
                .put(endpoint);
    }

    /**
     * Deletes an employee record based on the provided employee ID.
     *
     * @param employeeId The ID of the employee record to be deleted.
     * @throws RuntimeException If there is an issue during the deletion process.
     */
    public void deleteEmployeeRecord(String employeeId){
        String endpoint = endpointBase + "delete/" + employeeId;

        SerenityRest.given()
                .when()
                .get(endpoint);
    }

    /**
     * Obtains the HTTP status code from the last executed request.
     *
     * @return The HTTP status code.
     */
    public int getStatusCode() {
        return SerenityRest.then().extract().statusCode();
    }

    /**
     * Validates the success status in the response and retrieves the corresponding status value.
     *
     * @return The success status value extracted from the response.
     */
    public String validateSuccessInResponse() {
        return SerenityRest.then().extract().path("status");
    }
}
