package org.api.pages;

import io.restassured.path.json.JsonPath;
import net.serenitybdd.rest.SerenityRest;
import org.api.Client;
import io.restassured.response.Response;

public class ApiPage {

    private final Client client;

    /**
     * Constructor of the ApiPage class.
     * Initializes a new instance of the class and creates an instance of the Client class.
     */
    public ApiPage() {
        this.client = new Client();
    }

    /**
     * Performs a GET operation to retrieve information for a specific employee based on the provided employee ID.
     *
     * @param id The ID of the employee for which information is to be retrieved.
     */
    public void performGETById(String id) {
        client.getEmployeeInformation(id);
    }

    /**
     * Performs a GET operation to retrieve information for all employees.
     */
    public void performGET() {
        client.getEmployees();
    }

    /**
     * Performs a POST operation to create a new employee record with the provided data.
     *
     * @param name   The name of the new employee.
     * @param salary The salary of the new employee.
     * @param age    The age of the new employee.
     */
    public void performPOST(String name, String salary, String age) {
        client.createNewRecord(name,salary,age);
    }

    /**
     * Performs a PUT operation to update an existing employee record with the provided data.
     *
     * @param id     The ID of the employee record to be updated.
     * @param name   The updated name of the employee.
     * @param salary The updated salary of the employee.
     * @param age    The updated age of the employee.
     */
    public void performPUT(String id,String name, String salary, String age) {
        client.updateRecord(id, name, salary, age);
    }

    /**
     * Performs a DELETE operation to remove an existing employee record based on the provided employee ID.
     *
     * @param employeeId The ID of the employee record to be deleted.
     */
    public void performDELETE(String employeeId){
        client.deleteEmployeeRecord(employeeId);
    }

    /**
     * Retrieves and verifies the status code of the most recent API response.
     *
     * @return The status code of the API response.
     */
    public int verifyStatusCode(){
        return client.getStatusCode();
    }

    /**
     * Retrieves and verifies the status in the most recent API response.
     *
     * @return The status retrieved from the API response.
     */
    public String verifyStatus(){
        return client.validateSuccessInResponse();
    }
}
