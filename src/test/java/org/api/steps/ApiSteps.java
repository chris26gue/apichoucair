package org.api.steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import net.thucydides.core.steps.ScenarioSteps;
import org.api.pages.ApiPage;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class ApiSteps extends ScenarioSteps {

    private final ApiPage apiPage;

    public ApiSteps() {
        this.apiPage = new ApiPage();
    }

    /**
     * Performs a GET operation to retrieve data for a single employee based on the provided employee ID.
     *
     * @param employeeId The ID of the employee for which data is to be retrieved.
     */
    @When("I perform a GET operation a single employee data by id {string}")
    public void performGET(String employeeId) {
        apiPage.performGETById(employeeId);
    }

    /**
     * Performs a GET operation to query information for all clients.
     */
    @When("I perform a GET operation to query all clients")
    public void get() {
        apiPage.performGET();
    }

    /**
     * Verifies that the status code of the API response matches the expected code.
     *
     * @param code The expected status code to compare against the actual status code.
     */
    @Then("the status code should be {string}")
    public void verifyStatusCode(String code) {
        try {
            assertEquals("El código de estado no coincide", code, Integer.toString(apiPage.verifyStatusCode()));
            System.out.println("El Codigo fue exitoso");
        } catch (AssertionError e) {
            System.err.println("La aserción falló: " + e.getMessage());
        }
    }

    /**
     * Verifies that the response contains the expected status.
     *
     * @param status The expected status to compare against the actual response status.
     */
    @And("the response should contain {string}")
    public void verifyContain(String status){
        assertEquals("Mensaje correcto", status, apiPage.verifyStatus());
    }

    /**
     * Makes a POST request to create a new employee record using the provided employee data.
     *
     * @param employeeData A list of maps containing employee data, where each map represents a set of employee attributes.
     *                     The first map in the list is used to extract the name, salary, and age for the new employee record.
     */
    @Given("I make a POST request to create a new employee data:$")
    public void iHaveTheFollowingEmployeeData(List<Map<String, String>> employeeData) {
        apiPage.performPOST(employeeData.get(0).get("name"),employeeData.get(0).get("salary"),
                employeeData.get(0).get("age"));
    }

    /**
     * Initiates an update for an employee record with the provided data.
     *
     * @param employeeData A list of maps containing employee data, where each map represents a set of employee attributes.
     *                     The first map in the list is used to extract the ID, name, salary, and age for the employee record update.
     */
    @Given("I want to update an employee record with the following data:$")
    public void iWantToUpdateAnEmployeeRecordWithTheFollowingData(List<Map<String, String>> employeeData) {
        apiPage.performPUT(employeeData.get(0).get("id"),employeeData.get(0).get("name"),
                employeeData.get(0).get("salary"),employeeData.get(0).get("age"));
    }

    /**
     * Retrieves information for an existing employee record based on the provided employee ID.
     *
     * @param employeeId The ID of the existing employee record.
     */
    @Given("I have an existing employee record with ID {string}")
    public void iHaveAnExistingEmployeeRecordWithID(String employeeId) {
        apiPage.performGETById(employeeId);
    }

    /**
     * Initiates a request to delete the employee record with the provided ID.
     *
     * @param employeeId The ID of the employee record to be deleted.
     */
    @When("I request to delete the employee record {string}")
    public void iRequestToDeleteTheEmployeeRecord(String employeeId) {
        apiPage.performDELETE(employeeId);
    }
}
