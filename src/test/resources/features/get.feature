Feature: Get a single employee data

  @getId
  Scenario Outline: Get a single employee data
    When I perform a GET operation a single employee data by id "<employeeId>"
    Then the status code should be "<code>"
    And the response should contain "<status>"

    Examples:
      | employeeId | code | status |
      | 1          | 200  | success |

