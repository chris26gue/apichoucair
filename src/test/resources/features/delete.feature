Feature: Delete an Employee Record

  Scenario Outline: Delete an existing employee record
    Given I have an existing employee record with ID "<employeeId>"
    When I request to delete the employee record "<employeeId>"
    Then the status code should be "<code>"
    And the response should contain "<status>"

    Examples:
      | employeeId | code | status  |
      | 719        | 200  | success |
      | 720        | 200  | success |
      | 721        | 200  | success |