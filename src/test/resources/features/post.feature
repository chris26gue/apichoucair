Feature: Create new record in database

  @post
  Scenario Outline: Create new record in the database
    Given I make a POST request to create a new employee data:
      | name   | salary | age |
      | <name> | <salary> | <age> |
    Then the status code should be "<code>"
    And the response should contain "<status>"


    Examples:
      | name      | salary | age | code | status |
      | Cristian  | 123    | 26  | 200  | success |