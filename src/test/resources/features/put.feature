Feature: Update an employee record

  @put
  Scenario Outline: Update an employee record
    Given I want to update an employee record with the following data:
      | id   | name   | salary | age |
      | <id> | <name> | <salary> | <age> |
    Then the status code should be "<code>"
    And the response should contain "<status>"

    Examples:
      | id  | name | salary | age | code | status |
      | 21  | test | 123    | 23  | 200  | success |